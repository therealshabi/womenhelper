package testing.example.com.womenhelper;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity1 extends AppCompatActivity {

    int level;
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
        @Override
        //When Event is published, onReceive method is called
        public void onReceive(Context c, Intent i) {
            //Get Battery %
            level = i.getIntExtra("level", 0);
            //Find the progressbar creating in main.xml
            ProgressBar pb = (ProgressBar) findViewById(R.id.progressbar);
            //Set progress level with battery % value
            pb.setProgress(level);
            //Find textview control created in main.xml
            TextView tv = (TextView) findViewById(R.id.textfield);
            //Set TextView with text
            tv.setText("Battery Level: " + Integer.toString(level) + "%");

            /*EditText messageView = (EditText)findViewById(R.id.editText);
            String messageText = messageView.getText().toString();

            SmsManager smsManager = SmsManager.getDefault();

            if(level<=batteryLevel) //implement an option to choose your own percentage
            {
                smsManager.sendTextMessage("8375875507", null, messageText, null, null);
                //Toast.makeText(this, "Message sent!", Toast.LENGTH_SHORT).show();
            }*/
        }

    };


    EditText mobileno, message;
    Button sendsms;
    NumberPicker numberpicker;
    int batteryLevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);


        //--------------------------------------------------------------------------------------------


        //Register the receiver which triggers event
        //when battery charge is changed
        registerReceiver(mBatInfoReceiver, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));


        //------------------------------------------------------------------------------------------------


        mobileno = (EditText) findViewById(R.id.editText);
        message = (EditText) findViewById(R.id.editText2);
        sendsms = (Button) findViewById(R.id.btnSave);

        //Performing action on button click
        sendsms.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String no = mobileno.getText().toString();
                String msg = message.getText().toString();

                //Getting intent and PendingIntent instance
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

                //Get the SmsManager instance and call the sendTextMessage method to send message
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(no, null, msg, pi, null);

                Toast.makeText(getApplicationContext(), "Message Sent successfully!",
                        Toast.LENGTH_LONG).show();

                startActivity(new Intent(getBaseContext(), MainActivity2.class));
            }
        });


        //------------------------------------------------------------------------------------
        numberpicker = (NumberPicker) findViewById(R.id.numberPicker1);

        numberpicker.setMinValue(0);

        numberpicker.setMaxValue(10);

        numberpicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                batteryLevel = newVal;
                //textview.setText("Selected Value is : " + newVal);
            }
        });


    }


}
