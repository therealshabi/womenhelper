package testing.example.com.womenhelper;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    EditText ed1, ed2, ed3, ed4, ed5;
    Button b1;

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String contact1 = "contact1";
    public static final String contact2 = "contact2";
    public static final String contact3 = "contact3";
    public static final String contact4 = "contact4";
    public static final String contact5 = "contact5";

    SharedPreferences sharedpreferences1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ed1 = (EditText) findViewById(R.id.editText);
        ed2 = (EditText) findViewById(R.id.editText2);
        ed3 = (EditText) findViewById(R.id.editText3);
        ed4 = (EditText) findViewById(R.id.editText4);
        ed5 = (EditText) findViewById(R.id.editText5);

        b1 = (Button) findViewById(R.id.button);
        sharedpreferences1 = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s1 = ed1.getText().toString();
                String s2 = ed2.getText().toString();
                String s3 = ed3.getText().toString();
                String s4 = ed4.getText().toString();
                String s5 = ed5.getText().toString();

                SharedPreferences.Editor editor = sharedpreferences1.edit();

                editor.putString(contact1, s1);
                editor.putString(contact2, s2);
                editor.putString(contact3, s3);
                editor.putString(contact4, s4);
                editor.putString(contact5, s5);

                editor.commit();
                Toast.makeText(MainActivity2.this, "saved", Toast.LENGTH_LONG).show();
                displayContacts();
            }
        });

        displayContacts();
    }

    public void displayContacts() {
        String ss1 = sharedpreferences1.getString("contact1", null);
        String ss2 = sharedpreferences1.getString("contact2", null);
        String ss3 = sharedpreferences1.getString("contact3", null);
        String ss4 = sharedpreferences1.getString("contact4", null);
        String ss5 = sharedpreferences1.getString("contact5", null);

        TextView t1 = (TextView) findViewById(R.id.contactsDisplay1);
        TextView t2 = (TextView) findViewById(R.id.contactsDisplay2);
        TextView t3 = (TextView) findViewById(R.id.contactsDisplay3);
        TextView t4 = (TextView) findViewById(R.id.contactsDisplay4);
        TextView t5 = (TextView) findViewById(R.id.contactsDisplay5);
        t1.setText(ss1);
        t2.setText(ss2);
        t3.setText(ss3);
        t4.setText(ss4);
        t5.setText(ss5);
    }
    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }*/
}
